package WebService::ValidSign::API::Auth;
our $VERSION = '0.005';
use Moo;

# ABSTRACT: Implementation of the Authentication tokens for ValidSign

use Types::Standard qw(Str);
use namespace::autoclean;

has action_endpoint => (
    is      => 'ro',
    default => 'authenticationTokens'
);

has token => (
    is        => 'rw',
    isa       => Str,
    accessor  => '_token',
    predicate => 'has_token',
);

sub token {
    my $self = shift;

    if ($self->has_token) {
        return $self->_token;
    }
    else {
        return $self->_get_user_token;
    }
}

sub _get_user_token {
    my $self = shift;

    my $uri = $self->get_endpoint($self->action_endpoint, 'user');
    my $request = HTTP::Request->new(
        POST => $uri,
        [
            'Content-Type' => 'application/json',
            Accept         => 'application/json',
        ]
    );

    $request->header("Authorization", join(" ", "Basic", $self->secret));

    $self->_token($self->call_api($request)->{value});
    return $self->_token;
}

with "WebService::ValidSign::API";
has '+auth' => (required => 0);

__PACKAGE__->meta->make_immutable;

__END__

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head1 METHODS
