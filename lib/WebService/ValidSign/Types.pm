package WebService::ValidSign::Types;
our $VERSION = '0.005';
use warnings;
use strict;

# ABSTRACT: Moo(se) like types defined for WebService::ValidSign

use Types::Standard qw(Str);
use URI;
use Type::Utils -all;
use Type::Library
    -base,
    -declare => qw(
        WebServiceValidSignURI
        WebServiceValidSignAuthModule
    );

class_type WebServiceValidSignURI, {class => 'URI' };
coerce WebServiceValidSignURI, from Str, via { return URI->new($_); };

class_type WebServiceValidSignAuthModule, {class => 'WebService::ValidSign::API::Auth' };

1;

__END__

=head1 DESCRIPTION

Defines custom types for WebService::ValidSign modules

=head1 SYNOPSIS

    package Foo;
    use Moo;

    use WebService::ValidSign::Types qw(WebServiceValidSignURI);

    has bar => (
        is => 'ro',
        isa => WebServiceValidSignURI,
    );


=head1 TYPES

=head2 WebServiceValidSignURI

Allows a scalar URI, eq 'https://foo.bar.nl', or a URI object.

=cut
