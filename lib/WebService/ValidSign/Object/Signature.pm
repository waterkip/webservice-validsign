package WebService::ValidSign::Object::Signature;
our $VERSION = '0.005';
use Moo;

# ABSTRACT: A ValidSign Signature object
#
has a_set_of_fields => (
    is       => 'rw',
);

has data_specific_to_a_particular_signature => (
    is     => 'rw',
);

__PACKAGE__->meta->make_immutable;

__END__

