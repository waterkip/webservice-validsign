package WebService::ValidSign::Object::Ceremony;
our $VERSION = '0.005';
use Moo;
extends 'WebService::ValidSign::Object';

# ABSTRACT: A ceremony object

use Types::Standard qw(Bool);

has '+type' => ( required => 0 );

has in_person => (
    is => 'rw',
    isa => Bool,
    default => 0,
);

__PACKAGE__->meta->make_immutable;

__END__

