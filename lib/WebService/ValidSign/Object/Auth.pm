package WebService::ValidSign::Object::Auth;
our $VERSION = '0.005';
use Moo;
extends 'WebService::ValidSign::Object';

# ABSTRACT: A Signer auth object (whatever that may be)

use Types::Standard qw(Str ArrayRef);

has '+type' => (required => 0);

has challenges => (
    is      => 'rw',
    isa     => ArrayRef[Str],
    default => sub { [] }
);

has scheme => (
    is      => 'rw',
    isa     => Str,
    default => 'NONE'
);

__PACKAGE__->meta->make_immutable;

__END__

