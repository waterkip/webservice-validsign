package WebService::ValidSign::Object::Signer;
our $VERSION = '0.005';
use Moo;

extends 'WebService::ValidSign::Object::Sender';
use WebService::ValidSign::Object::Auth;

# ABSTRACT: A ValidSign signer object

has phone => (
    is => 'rw'
);

has auth => (
    is      => 'rw',
    lazy    => 1,
    builder => 1,
);

has knowledge_based_authentication => (
    is => 'rw',
);

sub _build_auth {
    return WebService::ValidSign::Object::Auth->new();

}


__PACKAGE__->meta->make_immutable;

__END__
