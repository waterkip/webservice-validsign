package WebService::ValidSign::Object::Document;
our $VERSION = '0.005';
use Moo;
extends 'WebService::ValidSign::Object';

# ABSTRACT: A ValidSign Document object
#
use Types::Standard qw(Str);

has '+type' => (required => 0);

has name => (
    is       => 'rw',
    isa      => Str,
    required => 1,
);

has path => (is => 'ro');

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $rv = $orig->($self, @_);
    delete $rv->{path};
    return $rv;
};

__PACKAGE__->meta->make_immutable;

__END__

